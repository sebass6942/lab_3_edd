public class ArbolBi {
    Nodo raiz;
    
    public ArbolBi() {
        raiz=null;
    }
      
      //METODO INSERTAR
      public void insertar (int info)
      {
          Nodo nuevo;  //crear un nodo
          nuevo = new Nodo (info);  //guardamos informacion en nuevo nodo
          nuevo.izq = null; //izquierda null
          nuevo.der = null; //derecha null
          if (raiz == null) //si raiz esta vacio entonces:
              raiz = nuevo; //raiz es igual a nuevo
          else  //si tiene datos entonces:
          {if (buscarNodo(info)==null) {    //para que no se repita el nodo

              Nodo anterior = null, reco;
              reco = raiz;
              while (reco != null)  //mientras reco sea distinto de null hacer:
              {
                  anterior = reco; // anterior es igual a reco
                  if (info < reco.info) // si informacion del nodo es menor que a la del nodo reco entonces:
                      reco = reco.izq; // reco es igual a reco izquierdo
                  else
                      reco = reco.der; // si  no es iugal a reco derecho.
              }
              if (info < anterior.info){ // si la informacion es menor a anterior informacion entonces:
                  anterior.izq = nuevo; //anterior izquierdo guarda el nodo nuevo
                  System.out.println("!!Nodo insertado¡¡");
              }else{
                  anterior.der = nuevo; // si no, guarda en el derecho
                  System.out.println("!!Nodo insertado¡¡");
              }
           }else{
                      System.out.println("!!El nodo ya se encuentra en el Arbol¡¡");
           }
          
          }
      }
      
      //METODO PARA SABER SI EL ARBOL ESTA VACIO
      public boolean esVacio(){
        return (raiz == null);
    }

      //METODO IMPRIMIR ARBOL EN PREORDER
      private void imprimirPre (Nodo reco)
      {
          if (reco != null)
          {
              System.out.print(reco.info + " ");
              imprimirPre (reco.izq);
              imprimirPre (reco.der);
          }
      }

      public void imprimirPre ()
      {
          imprimirPre (raiz);
          System.out.println();
      }

      //METODO IMPRIMIR ARBOL EN INORDER
      private void imprimirEntre (Nodo reco)
      {
          if (reco != null)
          {    
              imprimirEntre (reco.izq);
              System.out.print(reco.info + " ");
              imprimirEntre (reco.der);
          }
      }

      public void imprimirEntre ()
      {
          imprimirEntre (raiz);
          System.out.println();
      }

      //METODO IMPRIMIR ARBOL EN POSTORDER
      private void imprimirPost (Nodo reco)
      {
          if (reco != null)
          {
              imprimirPost (reco.izq);
              imprimirPost (reco.der);
              System.out.print(reco.info + " ");
          }
      }

      public void imprimirPost ()
      {
          imprimirPost (raiz);
          System.out.println();
      }
      
      //METODO PARA BUSCAR UN NODO EN EL ARBOL
      public  Nodo buscarNodo(int d){
          Nodo aux =raiz;  //apuntamos el nodo aux a raiz
          while(aux.info!=d){  // el dato del nodo aux es distinto de del dato recivido d entonces:
              if (d<aux.info) { //si el dato recivido es menor que el dato del nodo entonces:
                  aux=aux.izq; //aux es igual al aux izquierdo
              }else{  // si no 
                  aux=aux.der; //aux es igual a aux derecho
              }
              if (aux==null) {  //si el aux es igual a null
                  return null; //entonces retornamos null
              }
          }
          return aux;  //retorna aux
      }
      //METODO PARA ELIMINAR UN NODO DEL ARBOL
      public boolean eliminar(int d){
          Nodo aux=raiz; // creeamos 2 nods uno aux y otro padre que apunten a raiz
          Nodo padre=raiz;
          boolean esHijoIzq=true; //y un boolean que sea hijo izquierdo y si es falso sera derecho
          while(aux.info!=d){ //mientras el dato sea distinto del dato ingresado entonces:
              padre=aux;  //padre es igual a aux
              if (d<aux.info) { //si el dato recibido es menor que dato del arbol entonces es hijo izquierdo
                  esHijoIzq=true;
                  aux=aux.izq;
              }else{            //si no, es hijo derecho
                  esHijoIzq=false; // es hijon derecho
                  aux=aux.der; // aux es igual a aux derecho
              }
              if (aux==null) { // si aux es null entonces retornamos falsoooo
                  return false;
              }
          }//fin del while
          if (aux.izq==null && aux.der==null) { //si hijo derecho e hijo izquiero apuntan a null quiere decir que es hoja o raiz
              if (aux==raiz) {
                  raiz=null;
              }else if (esHijoIzq) { // si es hijo izquierdo
                  padre.izq=null; // padre izquierdo es igual a null
              }else{
                  padre.der=null; // padre derecho es igual a null
              }
          }else if (aux.der==null) { //si el aux de hijo derecho es igual a null entonces:
              if (aux==raiz) {
                  raiz=aux.izq; //aux apunta a hijo izquierdo
              }else if (esHijoIzq) { //si es hijo izq
                  padre.izq=aux.izq;
              }else{
                  padre.der=aux.izq;
              }
          }else if (aux.izq==null) { //si aux de hijo izquierdo es null entonces:
              if (aux==raiz) {
                  raiz=aux.der; //la raiz es igual a aux de hijo derecho
              }else if (esHijoIzq) { //si no si es hijo izquierdo entonces:
                  padre.izq=aux.der;  // padre izquierdo es igual a aux derecho
              }else{  // si no
                  padre.der=aux.der; // padre derecho es igual a aux derecho
              }
          }else{ // si no 
              Nodo reemplazo=obtenerNodoReemplazo(aux);  // el nodo reemplazo es igual al metodo Obtener Reempplazo con su dato
              if (aux==raiz) { //si aux es igual a la raiz entonces
                  raiz=reemplazo;  // raiz es igual a reemplazo
              }else if (esHijoIzq) { //si no si es hijo izquierdo
                  padre.izq=reemplazo; //padre izquierdo es igual a reemplazo
              }else{ //si no 
                  padre.der=reemplazo;// padre derecho es igual a reemplazo
              }
              reemplazo.izq=aux.izq; //remplazo izquierdo es igual a aux izquierdo
          }
          return true; //retornamos truee
      }
      
      //metododo encargado de devolver el nodo reemplazoooo
      public Nodo obtenerNodoReemplazo(Nodo NReemplazo){
          Nodo reemplazarPadre=NReemplazo;  //creamos 3 nodos para realizar el remplazo
          Nodo reemplazo=NReemplazo;
          Nodo aux=NReemplazo.der; //el nodo aux apunta al remplazo derecho
          while(aux!=null){ //mientras aux sea distinto de null entonces:
              reemplazarPadre=reemplazo;  //padre es igual a remplazo
              reemplazo=aux; //remplazo igual a aux
              aux=aux.izq;  //aux es igual a aux izquierdo
          }
          if (reemplazo!=NReemplazo.der) {   //si remaplzo es distinto del reemplazo derecho entonces:
              reemplazarPadre.izq=reemplazo.der;  //padre izquierdo es igual a reemplazo derecho
              reemplazo.der=NReemplazo.der;  //y remplazo derecho es igual a reemplazo der del Nodo recivido
          }
          System.out.println("El nodo Reemplazo es "+reemplazo);
          return reemplazo;   //retornamos reemplazo
      }
}
