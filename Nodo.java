public class Nodo {
    int info;
    Nodo izq, der;
    
    public Nodo(int info) {
        this.info = info;
        this.izq = izq;
        this.der = der;
    }

    public int getInfo() {
        return info;
    }

    public void setInfo(int info) {
        this.info = info;
    }

    public Nodo getIzq() {
        return izq;
    }

    public void setIzq(Nodo izq) {
        this.izq = izq;
    }

    public Nodo getDer() {
        return der;
    }

    public void setDer(Nodo der) {
        this.der = der;
    }
    //METODO TOSTRING
    @Override
    public String toString() {
        return "Nodo: " + info + ".";
    }
    
}
