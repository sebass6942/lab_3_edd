import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.*;

/**
 *
 * @author sebastian
 */
public class ArbolBinario {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));
        ArbolBi abo = new ArbolBi ();
        abo.insertar (100);
        abo.insertar (50);
        abo.insertar (25);
        abo.insertar (75);
        abo.insertar (150);
        abo.insertar (120);
        abo.insertar (10);
        abo.insertar (20);
        abo.insertar (40);
        abo.insertar (130);
        abo.insertar (200);
        int opc,p; 
            
            do
            {   //MENUUUU
                System.out.print("\n  --------MENU-------- ");  
                System.out.print("\n  | 1.Insertar  Nodo ");   
                System.out.print("\n  | 2.Eliminar  Nodo ");
                System.out.print("\n  | 3.Mostrar Preorden ");
                System.out.print("\n  | 4.Mostrar Inorden ");
                System.out.print("\n  | 5.Mostrar Postorden ");
                System.out.print("\n  | 6.Buscar Nodo");                
                System.out.print("\n  | 7.Salir ");
                System.out.print("\n Selecciona una opcion: "); 
                
                //variable del switch
                opc=Integer.parseInt(entrada.readLine());
                                      
                switch(opc)  
                 {
                  case 1: System.out.print("\n Ingrese nuevo Nodo: ");
                          p =Integer.parseInt(entrada.readLine());                         
                          abo.insertar(p);    //insertar dato
                          
                      break;
                    
                  case 2: if (!abo.esVacio()) {   //si el arbol NO esta vacio entonces:
                            System.out.print("\n Ingrese Nodo a eliminar: "); 
                            p=Integer.parseInt(entrada.readLine()); 
                                if (abo.eliminar(p)==false) {
                                    System.out.println("!!El Nodo no se encuentra en el Arbol¡¡");
                          }else{
                                    System.out.println("!!El Nodo a sido Eliminado del Arbol¡¡");
                                    abo.eliminar(p);  //elimina el nodo
                                }      
                           }else{
                                System.out.println("!!El Arbol esta VACIO¡¡");
                           }
                      break;
                  
                  case 3: System.out.println ("Impresion preorden: ");
                          abo.imprimirPre ();  //imprime preorden
                      break;
                     
                  case 4: System.out.println ("Impresion inorden: ");
                          abo.imprimirEntre (); //imprime inorden
                      break;
                      
                  case 5: System.out.println ("Impresion postorden: ");
                          abo.imprimirPost (); //imprime postorden
                      break;
                      
                  case 6: if (!abo.esVacio()) { //si el arbol NO esta vacio
                            System.out.print("\n Ingrese Nodo a Buscar: "); 
                            p=Integer.parseInt(entrada.readLine()); 
                            abo.buscarNodo(p); //busca el nodo ingresado
                            if (abo.buscarNodo(p)==null) {  //si es null no se encuentra
                                System.out.println("!!No se encontro el Nodo¡¡");
                                
                            }else{
                                System.out.println("!!Nodo Encontrado¡¡, sus datos son: "+abo.buscarNodo(p));
                            }
                        }else{
                            System.out.println("!!El Arbol esta VACIO¡¡");
                        }
                      break;                     
                default:    System.out.print("\nTERMINADO");
                            
            }
      }
            while(opc<=6);            
    } 
    
}
